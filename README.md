# cli-timetable
Simple python script that fetches iCalendar data from a URL and prints
it out in a fairly hopefully pretty fashion. Designed with welcome
screen terminals in mind.

## Usage
First the usual: get into your favourite virtual environment and install
required packages from `requirements.txt`. Copy `timetable.py.conf` over 
to `~/.config/` and modify the iCalendar source URL and your timezone
accordingly.

Run the `timetable.py` script and voilà! Good luck being up to date with
your commitments.
