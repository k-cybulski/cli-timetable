#!/usr/bin/env python3

import sys
from os.path import expanduser
from pytz import utc, timezone # timezones
from datetime import datetime, timedelta, date, time
import requests # downloading stuff from the web
import ics # iCalendar parsing as documented here https://icspy.readthedocs.io/en/v0.4/api.html
import colored # cute displays
from configparser import ConfigParser

### CONFIGS ###

DEFAULT_STYLE = colored.fg(255)

# Find the config file ~/.config/timetable.py.conf
# if it doesn't exist, use the config in this directory
config_path = expanduser("~/.config/timetable.py.conf")
try:
    file_ = open(config_path, 'r')
    file_.close()
except FileNotFoundError:
    config_path = "timetable.py.conf"

# Parse configs
parser = ConfigParser()
parser.read(config_path)
if len(sys.argv) > 1:
    CALENDAR_URL = sys.argv[1]
else:
    CALENDAR_URL = parser['Calendar']['url']
TIMEZONE = timezone(parser['Calendar'].get('timezone', 'Europe/Amsterdam'))

### HELPER CLASSES & FUNCTOINS ###

class Event:
    """
    Contains an event with a time range and additional info. It is
    constructed from an ics.Event, because those events use arrow 
    for datetime and are thus nasty. :<
    """
    def __init__(self, ics_event):
        self.begin = ics_event.begin.datetime
        self.end = ics_event.end.datetime
        self.name = ics_event.name
        self.location = ics_event.location

    def style(self):
        style = DEFAULT_STYLE
        if "lecture" in self.name.lower():
            style = colored.fg('sky_blue_2')

        if "project" in self.name.lower():
            style = colored.fg('orange_red_1')

        if "exam" in self.name.lower():
            style = colored.attr("blink") + colored.fg('red_1')
        return style

    def __str__(self, indent=""):
        str_ = indent + self.name + "\n"
        str_ += indent + "{} to {}".format(self.begin.strftime("%H:%M"),
                                  self.end.strftime("%H:%M"))
        if self.location: # if not empty
            str_ += " at {}".format(self.location)
        return str_

class Day:
    """
    Contains a date and all the events of a given day, enough to print 
    it out nicely. Includes helper functions for adding new events of 
    the ics event class.
    """
    def __init__(self, date_, events=[]):
        self.date = date_
        self.events = [] # tuple of (begin time, event) pairs
        self.add_events(events)

    def add_event(self, event):
        i = 0
        while i < len(self.events):
            if event.begin < self.events[i].begin:
                break
            i += 1
        self.events.insert(i, event) # i is kept in memory

    def add_events(self, events):
        for event in events:
            self.add_event(event)

    def print(self):
        date_string = self.date.strftime("%A %d of %B")
        print(colored.stylize(date_string, DEFAULT_STYLE))
        for event in self.events:
            print(colored.stylize(event.__str__(indent=" ") + "\n",
                                  event.style()))
        
# Define all utility functions
def humanize_datetime(datetime_, day=True):
    if day:
        format_string = "%A %d of %B "
    else:
        format_string = ""
    format_string += "%H:%M"
    return datetime_.strftime(format_string)

def clean_events(events):
    sorted_ = sorted(events, key=lambda event: event.begin)

    last_midnight = TIMEZONE.localize(datetime.combine(date.today(), time()))
    for i in range(len(sorted_)):
        # Skip events earlier than today
        # and those longer than a day
        if (sorted_[i].end > last_midnight
                and sorted_[i].end - sorted_[i].begin < timedelta(days=1)):
            return sorted_[i:]
    return []

def group_days(events):
    """
    Returns an ordered list of Day objects, takes an ordered list
    of Event objects as an argument.
    """
    days = []
    for event in events:
        event_date = event.begin.date()
        if len(days) == 0:
            days.append(Day(event_date))
        if event_date > days[-1].date:
            days.append(Day(event_date))
        days[-1].add_event(event)
    return days

### ACTUAL WORK ###

# Get the events and clean them (i.e. import them to our format and
# remove past events)
try:
    cal = ics.Calendar(requests.get(CALENDAR_URL).text)
    events = clean_events([Event(event) for event in cal.events])
except requests.exceptions.ConnectionError:
    print(("Error: Unable to connect to calendar API endpoint. Please check if"
           "the URL is correct."))
    exit(1)
except ics.parse.ParseError:
    print(("Error: Unable to parse data from calendar URL. Please check if the "
           "URL is correct."))
    exit(1)

# Group into days
days = group_days(events)

for day in days:
    day.print()
